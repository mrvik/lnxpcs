// ==UserScript==
// @name         Whatsapp Web background
// @namespace    https://web.whatsapp.com
// @version      0.1
// @description  Change Whatsapp Web background image
// @author       You
// @match        https://web.whatsapp.com/
// @grant        none
// ==/UserScript==

/* *
   * This script allows you to change Whatsapp Web background image.
   * To use this you should have a script injector like Tampermonkey or Greasemonkey.
   * Change the url var to your favourite image. 
   * You can also change the background-position or size or add any property you want.
   *
*/

(function() {
    'use strict';
    let url="https://gitlab.com/jstpcs/lnxpcs/raw/master/non-distro/open-world.png?inline=false";
    let content=`
      [data-asset-chat-background]{
        background-image: url(${url}) !important;
        background-position: top !important;
        background-size:cover !important;
      }`;
    let element=document.createElement("style");
    element.innerHTML=content;
    document.querySelector("head").appendChild(element);
})();